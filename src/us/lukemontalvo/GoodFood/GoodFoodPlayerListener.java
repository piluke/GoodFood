package us.lukemontalvo.GoodFood;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import org.bukkit.Material;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Cake;
import org.bukkit.inventory.ItemStack;

public class GoodFoodPlayerListener implements Listener {
	private GoodFood gf = null;

	public GoodFoodPlayerListener(GoodFood _gf) {
		super();
		gf = _gf;
	}

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerInteract(PlayerInteractEvent event) {
		GoodFood.log("onPlayerInteract start", true);

		Player player = event.getPlayer();
		GoodFood.log(String.format("onPlayerInteract: %s", this.gf.getPlayerEnabled(player) ? "enabled": "disabled"), true);
		if (!this.gf.getPlayerEnabled(player)) {
			return;
		}

		Action action = event.getAction();
		if ((action != Action.RIGHT_CLICK_AIR)&&(action != Action.RIGHT_CLICK_BLOCK)) { // Do nothing if right-click wasn't pressed
			GoodFood.log("no right click", true);
			return;
		}

		if ((!event.hasBlock())&&(action == Action.RIGHT_CLICK_BLOCK)) { // FIXME: Do nothing when the player clicked on a non-existent block(?)
			GoodFood.log("no block", true);
			return;
		}

		if (
			(action == Action.RIGHT_CLICK_BLOCK)
			&&(event.getClickedBlock().getType() == Material.CAKE)
			&&((event.hasItem())||(player.getInventory().getItemInMainHand().getType() == Material.AIR))
		){
			handleCakeEat(player, event);
		} else if ((event.hasItem())&&(event.getItem().getType() != Material.CAKE)) {
			prepareEat(player, event);
		}

		GoodFood.log("onPlayerInteract end", true);
	}
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerItemConsumeEvent(PlayerItemConsumeEvent event) {
		GoodFood.log("onPlayerItemConsumeEvent start", true);

		Player player = event.getPlayer();
		if (!this.gf.getPlayerEnabled(player)) {
			return;
		}

		ItemStack item = event.getItem();
		Material type = item.getType();
		int health = GoodFood.getFoodHealth(item);

		if ((health == 0)||(!canEat(player, health))) {
			if (
				(type != Material.GOLDEN_APPLE)
				&&(type != Material.MILK_BUCKET)
				&&(type != Material.POTION)
				&&(type != Material.SPLASH_POTION)
			) {
				GoodFood.log(String.format("consumed: hp = %d, type = %s", health, type), true);
				event.setCancelled(true);
				return;
			}
		}

		addHealth(player, health);
		player.setFoodLevel(20);

		GoodFood.log("onPlayerItemConsumeEvent end", true);
	}

	private boolean canEat(Player player, int health) {
		if ((health > 0)&&(player.getHealth() >= 20)) {
			return false;
		}
		if ((health < 0)&&(player.getHealth() + health <= 0)) {
			return false;
		}
		return true;
	}
	private void addHealth(Player player, int health) {
		int new_health = (int) Math.min(20, player.getHealth() + health);
		player.setHealth(new_health);
	}

	private void prepareEat(Player player, PlayerInteractEvent event) {
		GoodFood.log("prepareEat start", true);

		ItemStack item = event.getItem();
		Material type = item.getType();
		int health = GoodFood.getFoodHealth(item);
		if ((health == 0)||(!canEat(player, health))) {
			if (
				(type != Material.GOLDEN_APPLE)
				&&(type != Material.MILK_BUCKET)
				&&(type != Material.POTION)
				&&(type != Material.SPLASH_POTION)
			) {
				GoodFood.log(String.format("prepare: hp = %d, type = %s", health, type), true);
				return;
			}
		}

		player.setFoodLevel(19);

		GoodFood.log("prepareEat end", true);
	}
	private void handleCakeEat(Player player, PlayerInteractEvent event) {
		GoodFood.log("handleCakeEat start", true);

		int health = GoodFood.getFoodHealth(Material.CAKE);
		if ((health == 0)||(!canEat(player, health))) {
			GoodFood.log(String.format("cake hp = %d", health), true);
			return;
		}

		addHealth(player, health);

		Cake c = (Cake) event.getClickedBlock().getBlockData();
		if (c.getBites() == c.getMaximumBites()) {
			event.getClickedBlock().setType(Material.AIR);
		} else {
			c.setBites(c.getBites()+1);
			event.getClickedBlock().setBlockData((BlockData) c);
		}

		GoodFood.log("handleCakeEat end", true);
	}

	@EventHandler(priority=EventPriority.NORMAL)
	public void onHandChange(PlayerItemHeldEvent event) {
		Player p = event.getPlayer();
		if (this.gf.getPlayerEnabled(p)) {
			p.setFoodLevel(20);
			p.setSaturation(0.0f);
		}
	}
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player p = event.getPlayer();
		if (this.gf.getPlayerEnabled(p)) {
			p.setFoodLevel(20);
			p.setSaturation(0.0f);
		}
	}
}
