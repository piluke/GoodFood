package us.lukemontalvo.GoodFood;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

public class GoodFood extends JavaPlugin implements Listener {
	public static String version = "GoodFood v1.6";
	private static Logger logger = Logger.getLogger("minecraft");

	public FileConfiguration config = null;
	private File configurationFile = null;

	private static Random rand = new Random();

	public void onEnable() {
		getServer().getPluginManager().registerEvents(new GoodFoodEntityListener(this), this);
		getServer().getPluginManager().registerEvents(new GoodFoodPlayerListener(this), this);

		this.getCommand("goodfood").setExecutor(new GoodFoodCommand(this));

		createDefaultConfig(getDataFolder(), "config.yml");
		reloadConfig();

		log("Enabled!");
	}
	public void onDisable() {
		this.config = null;
		this.configurationFile = null;

		log("Disabled!");
	}

	public void reloadConfig() {
		if (this.configurationFile == null) {
			this.configurationFile = new File(getDataFolder(), "config.yml");
		}

		this.config = YamlConfiguration.loadConfiguration(this.configurationFile);
		InputStream defConfigStream = getResource("config.yml");
		if (defConfigStream != null) {
			InputStreamReader defConfigReader = new InputStreamReader(defConfigStream);
			YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigReader);
			this.config.setDefaults(defConfig);
		}
	}
	private void createDefaultConfig(File dir, String name) {
		if (!dir.exists()) {
			dir.mkdir();
		}

		File file = new File(dir, name);
		if (file.exists()) {
			return;
		}

		try {
			InputStream in = GoodFood.class.getResourceAsStream(String.format("/res/%s", name));
			if (in == null) {
				log("Failed to get default config file");
				return;
			}

			FileOutputStream out = new FileOutputStream(file);
			byte[] buffer = new byte[1024];
			int length;

			while ((length = in.read(buffer)) != -1) {
				out.write(buffer, 0, length);
			}

			out.close();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void saveConfig() {
		if ((this.config == null)||(this.configurationFile == null)) {
			return;
		}

		try {
			this.config.save(this.configurationFile);
		} catch (IOException ex) {
			log(String.format("Could not save config to %s: %s", this.configurationFile, ex.getMessage()));
		}
	}

	public static void log(String s, boolean is_debug_msg) {
		boolean is_debugging = false;
		//boolean is_debugging = true;
		if ((is_debugging)||(!is_debug_msg)) {
			GoodFood.logger.log(Level.INFO, String.format("[GoodFood] %s", s));
		}
	}
	public static void log(String s) {
		log(s, false);
	}
	public static void senderLog(CommandSender sender, Level level, String s) {
        if (level == Level.INFO) {
            s = String.format("%s%s", ChatColor.GREEN, s);
        } else if (level == Level.WARNING) {
            s = String.format("%s%s", ChatColor.RED, s);
        }
        sender.sendMessage(s);
    }

	public static int getRandomChance() {
		return rand.nextInt(100);
	}

	public boolean getPlayerEnabled(UUID u) {
		return this.config.getBoolean(String.format("players.%s.is_enabled", u), this.config.getBoolean("default_enabled", true));
	}
	public boolean getPlayerEnabled(Player p) {
		return getPlayerEnabled(p.getUniqueId());
	}

	public static int getFoodHealth(ItemStack item) {
		switch (item.getType()) {
			case APPLE: {
				return 4;
			}
			case BAKED_POTATO: {
				return 5;
			}
			case BEEF: {
				return 3;
			}
			case BEETROOT: {
				return 1;
			}
			case BEETROOT_SOUP: {
				return 6;
			}
			case BREAD: {
				return 5;
			}
			case CAKE: {
				return 2;
			}
			case CARROT: {
				return 3;
			}
			case CHICKEN: {
				return 2;
			}
			case CHORUS_FRUIT: {
				return 4;
			}
			// for CLOWNFISH see TROPICAL_FISH
			case COD: {
				return 2;
			}
			case COOKED_BEEF: {
				return 8;
			}
			case COOKED_CHICKEN: {
				return 6;
			}
			case COOKED_COD: {
				return 5;
			}
			case COOKED_MUTTON: {
				return 6;
			}
			case COOKED_PORKCHOP: {
				return 8;
			}
			case COOKED_RABBIT: {
				return 5;
			}
			case COOKED_SALMON: {
				return 6;
			}
			case COOKIE: {
				return 2;
			}
			case DRIED_KELP: {
				return 1;
			}
			// for ENCHANTED_GOLDEN_APPLE see GOLDEN_APPLE durability 1
			case GOLDEN_APPLE: {
				return 4;
			}
			case GOLDEN_CARROT: {
				return 6;
			}
			case MELON_SLICE: {
				return 2;
			}
			case MILK_BUCKET: {
				return 0;
			}
			case MUSHROOM_STEW: {
				return 6;
			}
			case MUTTON: {
				return 2;
			}
			case POISONOUS_POTATO: {
				return 2;
			}
			case PORKCHOP: {
				return 3;
			}
			case POTATO: {
				return 1;
			}
			case PUFFERFISH: {
				return 1;
			}
			case PUMPKIN_PIE: {
				return 8;
			}
			case RABBIT: {
				return 3;
			}
			case RABBIT_STEW: {
				return 10;
			}
			case ROTTEN_FLESH: {
				return 4;
			}
			case SALMON: {
				return 2;
			}
			case SPIDER_EYE: {
				return 2;
			}
			// for STEAK see COOKED_BEEF
			case TROPICAL_FISH: {
				return 1;
			}

			case POTION: {
				PotionMeta meta = (PotionMeta) item.getItemMeta();
				PotionData data = meta.getBasePotionData();
				
				if (data.getType() == PotionType.INSTANT_HEAL) {
					return 4;
				}
				
				return 0;
			}
			case SPLASH_POTION: {
				return 0;
			}
			default: {
				if (item.getType().isEdible()) {
					log(String.format("Unknown food type: %s", item.getType()));
					return 4;
				}
				return 0;
			}
		}
	}
	public static int getFoodHealth(Material type) {
		return getFoodHealth(new ItemStack(type, 1));
	}
}
