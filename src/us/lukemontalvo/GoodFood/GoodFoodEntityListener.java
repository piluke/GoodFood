package us.lukemontalvo.GoodFood;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class GoodFoodEntityListener implements Listener {
	private GoodFood gf = null;

	public GoodFoodEntityListener(GoodFood _gf) {
		super();
		gf = _gf;
	}

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onEntityRegainHealth(EntityRegainHealthEvent event) {
		if (!(event.getEntity() instanceof Player)) {
			return;
		}

		if (event.getRegainReason() == EntityRegainHealthEvent.RegainReason.SATIATED) {
			if (
				(!this.gf.config.getBoolean("health_regen_enabled"))
				&&(this.gf.getPlayerEnabled((Player) event.getEntity()))
			) {
				event.setCancelled(true);
			}
		}
	}

	@EventHandler(priority=EventPriority.NORMAL)
	public void onFoodLevelChange(FoodLevelChangeEvent event) {
		if (!(event.getEntity() instanceof Player)) {
			return;
		}

		if (this.gf.getPlayerEnabled((Player) event.getEntity())) {
			event.setCancelled(true);
		}
	}

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onEntityDamage(EntityDamageEvent event) {
		if (!(event.getEntity() instanceof Player)) {
			return;
		}

		if (event.getCause() == EntityDamageEvent.DamageCause.STARVATION) {
			if (this.gf.getPlayerEnabled((Player) event.getEntity())) {
				event.setCancelled(true);
			}
		}
	}
}
